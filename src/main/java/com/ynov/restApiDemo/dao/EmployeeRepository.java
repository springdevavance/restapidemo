package com.ynov.restApiDemo.dao;

import com.ynov.restApiDemo.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    public List<Employee> findByManagerId(Long managerId);

}
