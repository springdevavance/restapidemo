package com.ynov.restApiDemo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ynov.restApiDemo.entities.Message;

public interface MessageRepository extends JpaRepository<Message, Integer> {
    // find =select
    public List<Message> findBySignature(String signature);

    public List<Message> findByContentContains(String cdc);

    public List<Message> findByContentContainsAndTitre(String cdc, String titre);

    // chercher un message dont le titre contient x et dont la signature contient y
    // via une requête HQL
    @Query("select m from Message m where m.titre like :x and m.signature like :y ")
    public List<Message> searchMessage(@Param("x") String cdc1, @Param("y") String cdc2);

}
